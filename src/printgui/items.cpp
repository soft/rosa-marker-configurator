#include "items.h"
#include <QLineEdit>
#include <QComboBox>
#include <QBoxLayout>
#include <QCheckBox>
#include <QSpinBox>
#include <QLabel>
#include <QTableWidget>
#include <QDate>
#include <QHeaderView>
#include <QDebug>
#include <QLineEdit>
#include <QCompleter>
#include <QSettings>
#include <QTextCodec>
#include <QProcess>
#include <QMessageBox>
#include <QStandardPaths>
#include <QFileInfo>

#define OP(a) {static bool once=true; if(once){qDebug()<<a;} once=false;}

QStringList Items::itemList;
//QMap<QString,QString> Items::titles;
QMap<QString,bool> Items::visible;
QMap<QString,bool> Items::editable;
QMap<QString,bool> Items::completable;
Items::UserType Items::userType=Items::X;

QSettings* Items::cfg;
QSettings* Items::gCfg;
bool Items::inited=Items::init();
bool Items::init()
{
    cfg=new QSettings(getUserConfigPath()+"/PrintMarker.conf",QSettings::IniFormat);
    cfg->setIniCodec(QTextCodec::codecForLocale());
    gCfg=new QSettings(QSettings::SystemScope,"rosa","PrintMarker");
    gCfg->setIniCodec(QTextCodec::codecForLocale());

    QString name;

    name="Гриф";            //  0
    itemList<<name;
    visible[name]=false;//Есть граф полностью чтоб показать пользователю
    editable[name]=false;
    completable[name]=false;

    name="Гриф полностью";  //  1
    itemList<<name;
    visible[name]=true;
    editable[name]=false;
    completable[name]=false;

    name="Пункты перечня";  //  2
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=true;

    name="Экземпляр";       //  3
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=false;

    name="Учетный номер";   //  4
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=false;

    name="Список экземпляров";//  5
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=true;

    name="Исполнитель и отправитель";//  6
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=true;

    name="Наличие черновика";//  7
    itemList<<name;
    visible[name]=true;
    editable[name]=true;

    name="Место хранения оригинала";//  8
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=true;

    name="Дата разработки";//  9
    itemList<<name;
    visible[name]=true;
    editable[name]=true;
    completable[name]=false;

    return true;
}
QWidget* Items::generateEditWidget(QWidget *parent)
{
    int index=itemList.indexOf(fieldName);
    QWidget*widget=nullptr;
    if(!isVisible())
        return widget;
    switch (index)
    {
    case 0: //"Гриф";            //  0
    {
        QComboBox* box=new QComboBox(parent);
        box->addItem(getCurrentGrifShort());
        box->setCurrentIndex(0);
        widget=box;
    }
        break;
    case 1://"Гриф полностью";  //  1
    {
        QComboBox* box=new QComboBox(parent);
        box->addItem(getCurrentGrif());
        box->setCurrentIndex(0);
        widget=box;
    }
        break;
    case 2://"Пункты перечня";  //  2
    {
        QLineEdit* edit=new QLineEdit(parent);
        edit->setValidator(new QRegExpValidator(QRegExp("[\\x0001-\\x007fа-яА-ЯЁё№]*"),edit));
        edit->setText(lastValue("pp", "п.  перечня ").toString());
        if(isAutoCompleteable())
            edit->setCompleter(createCompleter(edit,"pp"));
        widget=edit;
    }
        break;
    case 3://"Экземпляр";       //  3
    {
        QWidget* w=new QWidget(parent);
        QBoxLayout* l=new QHBoxLayout(w);
        l->setMargin(0);
        w->setLayout(l);
        QCheckBox* cb=new QCheckBox("Единственный",w);
        l->addWidget(cb);
        l->addWidget(new QLabel("№",w));
        QSpinBox* sb=new QSpinBox(w);
        sb->setMinimum(1);
        l->addWidget(sb);
        QObject::connect(cb,SIGNAL(clicked(bool)),sb,SLOT(setDisabled(bool)));
        cb->setChecked(lastValue("uno",false).toBool());
        sb->setValue(lastValue("number",1).toInt());
        sb->setDisabled(cb->isChecked());
        widget=w;
    }
        break;
    case 4://"Учетный номер";   //  4
    case 8://"Место хранения оригинала";//  8
    {
        QString name=(index==4)?"num":"place";
        QLineEdit* edit=new QLineEdit(parent);
        edit->setValidator(new QRegExpValidator(QRegExp("[\\x0001-\\x007fа-яА-ЯЁё№]*"),edit));
        edit->setText(lastValue(name,"").toString());
        widget=edit;
        if(isAutoCompleteable())
            edit->setCompleter(createCompleter(edit,name));
    }
        break;
    case 5://"Список экземпляров";//  5
    {
        QTableWidget* table=new QTableWidget(100,2,parent);
        table->verticalHeader()->setVisible(false);
        table->setColumnWidth(0,50);
        table->horizontalHeader()->setStretchLastSection(true);
        table->setHorizontalHeaderLabels(QStringList()<<"№"<<"Куда");
        table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        table->setEditTriggers(QAbstractItemView::AllEditTriggers);
        if(isAutoCompleteable())
            table->setItemDelegateForColumn(1,new ComleteDelegate(table));
        QVariantList last=lastValue("list",QVariant()).toList();
        for (int row=0; row < last.count()/2; row++)
        {
            table->setItem(row,0,new QTableWidgetItem(last.at(row*2+0).toString()));
            table->setItem(row,1,new QTableWidgetItem(last.at(row*2+1).toString()));
        }
        widget=table;
    }
        break;
    case 6://"Исполнитель и отправитель";//  6
    {
        QWidget* w=new QWidget(parent);
        QBoxLayout* l=new QHBoxLayout(w);
        l->setMargin(0);
        w->setLayout(l);
        l->addWidget(new QLabel("Исп.",w));
        QString user=getUserNmae();
        QLineEdit* edit1=new QLineEdit(w);
        edit1->setValidator(new QRegExpValidator(QRegExp("[\\x0001-\\x007fа-яА-ЯЁё№]*"),edit1));
        edit1->setText(lastValue("isp",user).toString());
        l->addWidget(edit1);
        l->addWidget(new QLabel("Отп.",w));
        QLineEdit* edit2=new QLineEdit(w);
        edit2->setValidator(new QRegExpValidator(QRegExp("[\\x0001-\\x007fа-яА-ЯЁё№]*"),edit2));
        edit2->setText(lastValue("otp",user).toString());
        l->addWidget(edit2);
        if(isAutoCompleteable())
        {
            edit1->setCompleter(createCompleter(edit1,"by"));
            edit2->setCompleter(createCompleter(edit2,"by"));
        }
        QObject::connect(edit1,SIGNAL(textEdited(QString)),edit2,SLOT(setText(QString)));
        widget=w;
    }
        break;
    case 7://"Наличие черновика";//  7
    {
        QCheckBox* cb=new QCheckBox("Есть",parent);
        cb->setChecked(lastValue("chern",false).toBool());
        widget=cb;
    }
        break;
    case 9://"Дата разработки";//  9
    {
        QLineEdit* edit=new QLineEdit(parent);
        edit->setValidator(new QRegExpValidator(QRegExp("[\\x0001-\\x007fа-яА-ЯЁё№]*"),edit));
        edit->setText(QDate::currentDate().toString());
        if(isAutoCompleteable())
            edit->setCompleter(createCompleter(edit,"date"));
        widget=edit;
    }
        break;

    default:
        break;
    }
    return widget;
}

QString Items::value(QWidget *widget)
{
    int index=itemList.indexOf(fieldName);
    QString value;
    switch (index)
    {
    case 0: //"Гриф";            //  0
        value=getCurrentGrifShort();
        break;
    case 1://"Гриф полностью";  //  1
        value=getCurrentGrif();
        break;
    case 2://"Пункты перечня";  //  2
    {
        QLineEdit* edit=qobject_cast<QLineEdit*>(widget);
        if(edit)
        {
            value= edit->text();
            setLastValue("pp",value);
            if(isAutoCompleteable())
                updateCompleterValue("pp",value);
        }
        else
            value= "нет пунктов перечня";
    }
        break;
    case 3://"Экземпляр";       //  3
    {
        if(!widget || !widget->layout() || widget->layout()->count()<3)
            return "плохо с экземплярами";
        QCheckBox* cb=qobject_cast<QCheckBox*>(widget->layout()->itemAt(0)->widget());
        QSpinBox* sb=qobject_cast<QSpinBox*>(widget->layout()->itemAt(2)->widget());
        if(cb->isChecked())
            value= "В единственном экземпляре";
        else
            value= "Экз. №"+QString::number(sb->value());
        setLastValue("uno",cb->isChecked());
        setLastValue("number",sb->value());
    }
        break;
    case 4://"Учетный номер";   //  4
    case 8://"Место хранения оригинала";//  8
    {
        QString name=(index==4)?"num":"place";
        QLineEdit* edit=qobject_cast<QLineEdit*>(widget);
        if(edit)
        {
            value=edit->text();
            if(isAutoCompleteable())
                updateCompleterValue(name,value);
            setLastValue(name,value);
        }
        if(index==4)
            value="Уч.№"+value;
    }
        break;
    case 5://"Список экземпляров";//  5
    {
        QTableWidget* table=qobject_cast<QTableWidget*>(widget);
        if(!table)
            return "плохо со списком экземпляров";
        QVariantList newData;
        for(int row=0;row<table->rowCount();row++)
        {
            if(!table->item(row,0) || !table->item(row,1))
                continue;
            QString col1=table->item(row,0)->text().trimmed();
            QString col2=table->item(row,1)->text().trimmed();
            if(col1.isEmpty() ||col2.isEmpty())
                continue;
            newData<<col1<<col2;
            if(isAutoCompleteable())
                updateCompleterValue("sendto",col2);
            if(!value.isEmpty())
                value+="\n";
            value+="Экз. "+col1+" в "+col2;
        }
        setLastValue("list",newData);
    }
        break;
    case 6://"Исполнитель и отправитель";//  6
    {
        if(!widget || !widget->layout() || widget->layout()->count()<4)
            return "плохо с экземплярами";
        QLineEdit* edit1=qobject_cast<QLineEdit*>(widget->layout()->itemAt(1)->widget());
        QLineEdit* edit2=qobject_cast<QLineEdit*>(widget->layout()->itemAt(3)->widget());
        if(!edit1 ||!edit2)
            return "плохо с исполнителем";
        QString isp=edit1->text().trimmed();
        QString otp=edit2->text().trimmed();
        setLastValue("isp",isp);
        setLastValue("otp",otp);
        if(isAutoCompleteable())
        {
            updateCompleterValue("by",isp);
            updateCompleterValue("by",otp);
        }
        if(isp==otp)
            value="Исп. и отп. "+isp;
        else
            value="Исп. "+isp+"\n"+"Отп. "+otp;
    }
        break;
    case 7://"Наличие черновика";//  7
    {
        QCheckBox* cb=qobject_cast<QCheckBox*>(widget);
        if(!cb)
            return "плохо с галочкой";
        setLastValue("chern",cb->isChecked());
        if(cb->isChecked())
            value="Б/ч,";
        else
            value="";
    }
        break;
    case 9://"Дата разработки";//  9
    {
        QLineEdit* edit=qobject_cast<QLineEdit*>(widget);
        if(!edit)
            return "плохо с датой";
        if(isAutoCompleteable())
            updateCompleterValue("date",edit->text());
        value=edit->text();
    }
        break;

    default:
        break;
    }
    return value;
}

bool Items::isSpecial()
{
    if(fieldName=="Список экземпляров")
        return true;
    else
        return false;
}

void Items::connTo(Items toField, QWidget* fromW, QWidget *toWidget)
{
    if(fieldName=="Экземпляр" && toField.fieldName=="Список экземпляров")
    {
        if(!fromW || !fromW->layout() || fromW->layout()->count()<3)
            return ;
        QCheckBox* cb=qobject_cast<QCheckBox*>(fromW->layout()->itemAt(0)->widget());
        QTableWidget* table=qobject_cast<QTableWidget*>(toWidget);
        if(!table)
            return ;
        ConnectorSE* c;
        QObject::connect(cb,SIGNAL(toggled(bool)),c=new ConnectorSE(table),SLOT(setOneRow(bool)));
        c->setOneRow(cb->isChecked());
    }

}

QVariant Items::lastValue(QString name, QVariant def) const
{
    return cfg->value("last/"+name,def);
}

void Items::setLastValue(QString name, QVariant value) const
{
    cfg->setValue("last/"+name,value);
    cfg->sync();
}

QString Items::getUserNmae()
{
    QProcess proc;
    proc.start("/bin/sh -c \"getent passwd $(whoami)|cut -d ':' -f 5\"");
    proc.waitForFinished(5000);
    if(proc.exitCode())
    {
        qWarning()<<"Error on get full user name "<<proc.exitCode()<<QString(proc.readAllStandardError());
        return getUser();
    }
    else
    {
        QString name=proc.readAllStandardOutput().trimmed();
        OP("Full user name from passwd: "<<name)
        if(name.isEmpty())
            return getUser();
        else
            return name;
    }
}

QString Items::getUser()
{
    QString name;
    QProcess proc;
    proc.start("whoami");
    proc.waitForFinished(5000);
    if(proc.exitCode())
    {
        qWarning()<<"Error on get whoami"<<proc.exitCode()<<QString(proc.readAllStandardError());
        name=qgetenv("USER");
        OP("Env user: "<<name)
    }
    else
    {
        name=proc.readAllStandardOutput().trimmed();
        OP("whoami: "<<name)
        if(name.isEmpty())
        {
            name=qgetenv("USER");
            OP("Env user: "<<name)
        }
    }
    return name;
}
#include <selinux/selinux.h>
#include <selinux/context.h>
#include <selinux/get_context_list.h>

QString Items::getCurrentGrif()
{
    static QString resRange;
    if(!resRange.isEmpty())
        return resRange;
    char* con=0;

    if(is_selinux_enabled())
        OP("is_selinux_enabled"<<"true")
    else
        {
            OP("is_selinux_enabled"<<"false")
            return getGrifList()[0];
        }
    //qDebug()<<"getCurrentGrif";
    //qDebug()<<get_default_context("user_u","s0-s3",&con)<< (void*)con;
    bool by_proc=true;
    if(by_proc)
    {
            QString pid;
            QProcess proc;
            //getuid();
            proc.start("bash -c \"ps -U $UID o pid,comm|grep startkde\"");
            proc.waitForFinished(5000);
            QString output;
            if(proc.exitCode())
            {
                qWarning()<<"Error on get ps"<<proc.exitCode()<<QString(proc.readAllStandardError());
            }
            else
            {
                output=proc.readAllStandardOutput().trimmed();
                //qDebug()<<"pids: "<<output;
                pid=output.mid(0,output.indexOf(' '));
                if(pid.isEmpty())
                {
                    OP("Can not found my startkde proces")
                        proc.start("bash -c \"ps -U $UID  o pid\"");
                        proc.waitForFinished(5000);
                        if(proc.exitCode())
                        {
                            qWarning()<<"Error on get ps"<<proc.exitCode()<<QString(proc.readAllStandardError());
                        }
                        else
                        {
                            proc.readLine();
                            pid=proc.readLine().trimmed();
                        }
                }
            }
            /*foreach(QString pid1, output.split(' '))
            {
                if(getpidcon(pid1.toInt(),&con)!=-1)
                    qDebug()<<pid1<<": "<<con;
            }*/
        OP("a PID of my user:"<<pid);
        if(pid.toInt())
        {
            if(getpidcon(pid.toInt(),&con)==-1)
            {
                con=0;
                OP("Can not getpidcon for "<<pid<<strerror(errno))
            }
        }
    }
    if(!con)
    {
        char* se_user;
        char* level;
        if(getseuserbyname(qPrintable(getUser()),&se_user,&level)==-1)
        {
            OP("Can not get seuser for "<<getUser()<<strerror(errno))
            return getGrifList()[0];
        }
        OP("getseuserbyname ("<<getUser()<<")"<<QString(se_user)<<"level:"<<level)

        //if(getcon(&con)==-1)
        if(get_default_context(se_user,NULL,&con)==-1)
        {
            OP("get_default_context for user"<<QString(se_user)<<"error"<<errno<<strerror(errno))
            if(con)
                freecon(con),con=0;
            if(getcon(&con)==-1)
            {
                OP("getcon error"<<errno<<strerror(errno))
                if(con)
                    freecon(con);
                return getGrifList()[0];
            }
            OP("Use getcon context:"<<con)
        }
        else
            OP("Use get_default_context context:"<<con)
    }

    context_t context=context_new(con);
    QString range=context_range_get(context);
    OP("user"<<context_user_get(context)<<endl<<
       "role"<<context_role_get(context)<<endl<<
        "type"<<context_type_get(context)<<endl<<
        "range"<<range<<endl<<
        "raw ="<<(char*)con)

    //char*qqq;
    //char**list;
    //get_ordered_context_list("",0,&list);
    //qDebug()<<"list"<<list<<list[0]<<list[1];
    if(range.contains(':'))
        range=range.mid(0,range.indexOf(':'));
    if(hasTranslation())
    {
        char* r2;
        if(range.contains('-'))
        {
            range=":::"+range;
            selinux_trans_to_raw_context(range.toUtf8().constData(),&r2);
            range=r2+3;
            OP("Un translated range"<<range)
            freecon(r2);
            if(range.contains(':'))
                range=range.mid(0,range.indexOf(':'));
            range=range.mid(range.indexOf('-')+1);
            OP("Last part of range"<<range)
        }
        range=":::"+range;
        selinux_raw_to_trans_context(range.toUtf8().constData(),&r2);
        OP("raw_to_trans range:"<<r2)
        range=r2+3;
        freecon(r2);

//        qDebug()<<"range transd "<<range;
    }
    else
    {
        if(range.contains('-'))
        {
//            qDebug()<<"contains -"<<range;
            range=range.mid(range.indexOf('-')+1);
            OP("Last part range (untranslatable):"<<range)
        }
//        qDebug()<<"range manual -"<<range<<selinux_translations_path();
        if(strlen(selinux_translations_path())>0)
        {
            OP("Found stanslation file: "<<selinux_translations_path())
            QSettings translations(selinux_translations_path(),QSettings::IniFormat);
            translations.setIniCodec(QTextCodec::codecForLocale());
            QString g=translations.value(range).toString();
            range=g.mid(0,g.indexOf('#'));
            OP("Manual translated range:"<<range)
        }
//        qDebug()<<"range manual"<<range;

    }
    freecon(con);
    OP("Result grif: "<<range)
    resRange=range;
    return resRange;
/*    qDebug()<<"back trans"<<selinux_trans_to_raw_context(range.toUtf8().constData(),&qqq)<<qqq;
    qDebug()<<"back trans2"<<selinux_trans_to_raw_context(con,&qqq)<<qqq;
    range=":::"+range;
    qDebug()<<"back trans3"<<selinux_trans_to_raw_context(range.toUtf8().constData(),&qqq)<<qqq;

    qDebug()<<selinux_translations_path();
    //qDebug()<<"tr?"<<qDebug()<<(context_range_get(context),&qqq)<<qqq;
    qDebug()<<"col?"<<selinux_raw_context_to_color(context_range_get(context),&qqq)<<qqq;
    for(int i=0;i<16;i++)
    {
        QString s=":::s"+QString::number(i);
        //s="Не_секретно";
        qDebug()<<s<<" -> "<<"?"<<selinux_raw_to_trans_context(s.toLatin1().constData(),&qqq)<<qqq;
    }
    qDebug()<<"stsc s1"<<string_to_security_class("s1");
    qDebug()<<"stsc DPS"<<string_to_security_class("DPS");
    qDebug()<<"scts 1"<< security_class_to_string(1);
    qDebug()<<"scts 2"<< security_class_to_string(2);
    //qDebug()<<"trans"<<qqq;
    qDebug()<<"ini trans"<<translations.value(context_range_get(context)).toString();
    freecon(qqq);
    if(range.startsWith("s",Qt::CaseInsensitive) && range.length()>=2 && range.at(1).isDigit())
    {
        QString g=translations.value(context_range_get(context)).toString();
        return g.mid(0,g.indexOf('#'));
    }
    else if(range.isEmpty())
    {
        qCritical()<<"se range is empty!";
        return getGrifList()[0];
    }
    else
    {
        return range;
    }*/
}

QString Items::getCurrentGrifShort()
{
    QString s=getCurrentGrif();
    QString r;
    if(s.contains(' '))
    {
        foreach (QString word, s.split(' ',QString::SkipEmptyParts)) {
            QString w=word[0].toUpper();
            if(w.startsWith("НЕ"))
            {
                r.append("Н");
                w=w.mid(2);
            }
            if(!w.isEmpty())
                r.append(w);
        }
        OP("Short grif space separated"<<r)
    }
    else
    {
        if(s.startsWith("НЕ",Qt::CaseInsensitive))
        {
            r.append("Н");
            s=s.mid(2);
        }
        for (int i=0;i<s.length();i++)
        {
            QChar c=s[i];
            if(i==0 || c.isUpper())
                r+=c;
        }
        OP("Short grif upper separated"<<r)
    }
    return r;
}
#include <QRegExp>
QStringList Items::getGrifList()
{
    //TODO get from SELinux
    QStringList list;
    if(hasTranslation())
    {
        for(int i=0;i<16;i++)
        {
            QString s=":::s"+QString::number(i);
            char* level;
            selinux_raw_to_trans_context(s.toLatin1().constData(),&level);
            if(s!=level)
                list<< (level+3);
            freecon(level);
        }
        OP("List grif by translations:"<<list)
    }
    else if(strlen(selinux_translations_path()))
    {
        QSettings translations(selinux_translations_path(),QSettings::IniFormat);
        translations.setIniCodec(QTextCodec::codecForLocale());
        QRegExp r("^[sS]\\d+$");
        foreach(QString key, translations.childKeys())
        {
            key=key.mid(0,key.indexOf('#'));
            if(r.indexIn(key)!=-1)
            {
                QString g=translations.value(key).toString();
                g=g.mid(0,g.indexOf('#'));
                list.append(g);
            }
        }
        if(list.isEmpty())
        {
            list.append("Не секретно");
            OP("Not found level list in"<<selinux_translations_path()<<", NS only:"<<list)
        }
        else
            OP("Manual found level list:"<<list)
    }
    else
    {
        for(int i=0;i<16;i++)
            list<<"s"+QString::number(i);
        OP("No any translations, using all 16:"<<list)
    }
    return list;
    //return QStringList()<<"Не секретно"<<"Для служебного пользования"<<"Секретно"<<"Совершенно секретно";
}

QCompleter *Items::createCompleter(QObject *parent, QString name)
{
    cfg->beginGroup("completer");
    QStringList list;
    int n=cfg->beginReadArray(name);
    for(int i=0;i<n;i++)
    {
        cfg->setArrayIndex(i);
        list<<cfg->value("v").toString();
    }
    cfg->endArray();
    cfg->endGroup();
    QCompleter* c=new QCompleter(list,parent);
    c->setCaseSensitivity(Qt::CaseInsensitive);
    c->setCompletionMode(QCompleter::PopupCompletion);
    c->setFilterMode(Qt::MatchContains);
    c->model()->sort(0,Qt::AscendingOrder);
    c->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    return c;
}

void Items::updateCompleterValue(QString name, QString value)
{
    cfg->beginGroup("completer");
    int n=cfg->beginReadArray(name);
    QMap<QString,int> values;
    for(int i=0;i<n;i++)
    {
        cfg->setArrayIndex(i);
        QString value=cfg->value("v").toString();
        int cnt=cfg->value("c").toInt()+1;
        if(cnt<100)
            values[value]=cnt;
    }
    cfg->endArray();
    values[value]=1;
    int i=0;
    cfg->beginWriteArray(name);
    foreach (QString s, values.keys()) {
        if(s.trimmed().isEmpty())
            continue;
        cfg->setArrayIndex(i++);
        cfg->setValue("v",s);
        cfg->setValue("c",values[s]);
    }
    cfg->endArray();
    cfg->endGroup();
    cfg->sync();
}
#include <unistd.h>
Items::UserType Items::getUserType()
{
    if(userType==X)
    {
        QStringList pathList=QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation);
        //qDebug()<<"getUserType X"<<userType<<pathList;
        foreach (QString path, pathList)
            if(path.startsWith("/etc"))
            {
                QFileInfo info(path);
                if(info.isWritable())
                {
                    userType=Admin;
                    qDebug()<<"getUserType - Admin ("<<path<<"writable)"<<path;
                }
                break;
            }
        if(userType==X)
        {            
            QVariantList grifs=Items::getGlobalCfg()->value("grifs").toList();
            OP("Force mark on levels "<<grifs<<"form"<<Items::getGlobalCfg()->fileName())
            QStringList levels=getGrifList();
            QString current=getCurrentGrif();
            int index=levels.indexOf(current);
            //qDebug()<<"getUserType XX"<<Items::getGlobalCfg()->allKeys()<<grifs<<levels<<current<<index<<Items::getGlobalCfg()->fileName();
            if(index<0 || index>=grifs.count())
            {
                if(index<0)
                    qCritical()<<"getUserType - current level not found in list - Simple";
                else
                    qCritical()<<"getUserType - current level index="<<index<<"in config"<<Items::getGlobalCfg()->fileName()<<"less count - Simple";
                userType=Simple;
            }
            else
            {
                bool edit=!grifs[levels.indexOf(current)].toBool();
                userType=edit?Editor:Simple;
                qDebug()<<"getUserType - "<<(edit?"Editor":"Simple");
            }
        }
    }
    return userType;
}

bool Items::hasTranslation()
{
    static bool once=false;
    if(!once && !is_selinux_enabled())
        QMessageBox::critical(nullptr,"Маркировка документа", "SE Linux не установлен. Работа программы может быть не корректна!");
    char*s;
    const char*raw=":::s0";
    selinux_raw_to_trans_context(raw, &s );
    //qDebug()<<"hasTranslation"<<"raw"<<raw<<"s"<<s;
    bool rez=strcmp(s,raw)!=0;
    if(!once && !rez)
        QMessageBox::critical(nullptr,"Маркировка документа", "Служба перевода mcstransd не работает. Работа программы может быть не корректна!");
    freecon(s);
    OP("hasTranslation:"<<rez)
    once=true;

    return rez;
}
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

QString Items::getUserConfigPath()
{
    struct passwd *pw = getpwuid(getuid());
    return QString(pw->pw_dir)+"/.config";
}
#include <QValidator>

QWidget *ComleteDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
    QLineEdit* le=new QLineEdit(parent);
    le->setValidator(new QRegExpValidator(QRegExp("[\\x0001-\\x007fа-яА-ЯЁё№]*"),le));
    le->setCompleter(Items::createCompleter(parent,"sendto"));
    return le;
}

QSet<QString> ComleteDelegate::tos;
void ComleteDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QLineEdit* le=qobject_cast<QLineEdit*>(editor);
    if(!le)
        return;
    le->setText(index.data(Qt::EditRole).toString());
}

void ComleteDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit* le=qobject_cast<QLineEdit*>(editor);
    model->setData(index,le->text().trimmed(),Qt::EditRole);
    tos<<le->text();
}

