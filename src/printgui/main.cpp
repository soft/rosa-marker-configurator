#include <QApplication>
#include <QDebug>
#include <QDateTime>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "printwidget.h"
#include "../admingui/editor.h"
#include "items.h"
//#include <QtGui>
//#include <QtWidgets>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context)
    switch (type) {
    case QtDebugMsg:
    case QtWarningMsg:
    case QtCriticalMsg:
    {
        foreach (QString msgl, msg.split('\n')) {
            QString str="| "+QDateTime::currentDateTime().toString("MM/dd/yy - HH:mmA")+" | printgui: "+msgl+"\n";
            fputs(qPrintable(str),stderr);
        }
        break;
    }
    case QtInfoMsg:
        //fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        //fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
}
int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageOutput);
    QApplication a(argc, argv);
    /*int newstderr = open("/var/log/printgui", O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP |     S_IROTH);
    if(newstderr<0)
        fclose(stderr);
    else
        dup2(newstderr, fileno(stderr));*/
    //qDebug()<<'\n'<<qPrintable(QString().fill('-',60))<<'\n'<<qPrintable(a.applicationName())<<qPrintable(QDateTime::currentDateTime().toString(Qt::ISODate));
    qDebug()<<qPrintable(a.applicationName())<<"Starting"<<qPrintable(QDateTime::currentDateTime().toString(Qt::ISODate));
    if(a.arguments().contains("admin"))
    {
        if(Items::getUserType()==Items::Admin)
        {
            Editor w;
            w.show();
            return a.exec();
        }
        else
            return 1;
    }
    else
    {
        PrintWidget w;
        w.show();
        return a.exec();
    }
}
