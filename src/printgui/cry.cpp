/* ************************************************************************************************** */
/*                                                                                                    */
/*                                                         :::::::::   ::::::::   ::::::::      :::   */
/*  cry.c                                                 :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                       +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>       +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                     +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2019/10/10 18:30:47 by Ivan Marochkin    #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2019/10/10 18:30:47 by Ivan Marochkin   ###    ###  ########   ########  ###     ###     */
/*                                                                                                    */
/* ************************************************************************************************** */
#include <time.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <stdlib.h>
/*
    Simple encryption algorithm.
*/

char *ft_decry(char const *buf, unsigned key) {

    char *res = (char *)malloc(sizeof(char) * (4096));
    int i = 0, j = 0;
    while (i < 4096) {
        res[i] = buf[i] + ((char *)&key)[j] + i * 42;
        ++j;
        if (j == 4) {
            j = 0;
        }
        if (res[i] == 0) {
            break;
        }
        ++i;
    }
    return (res);
}

char *ft_encry(char const *buf, unsigned key) {
    int len;
    char *res = (char *)malloc(sizeof(char) * (len=(strlen(buf)+1)));
    int i = 0, j = 0;
    while (i < len) {
        res[i] = key * key;
        ++i;
    }
    i = 0;
    while (buf[i]) {
        res[i] = buf[i] - ((char *)&key)[j] - i * 42;
        ++j;
        if (j == 4) {
            j = 0;
        }
        ++i;
    }
    res[i] = buf[i] - ((char *)&key)[j] - i * 42;
    return (res);
}

unsigned get_key(void) {

    struct sysinfo a1;
    time_t rr = time(0);
    sysinfo(&a1);
    unsigned key = ((rr - a1.uptime) / 100) << 4;
    return (key);
}
