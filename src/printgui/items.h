#ifndef ITEMS_H
#define ITEMS_H
#include <QStringList>
#include <QWidget>
#include <QMap>
#include <QTableWidget>
#include <QItemDelegate>
#include <QSettings>
#include <QCompleter>
#include <QVariant>

class ConnectorSE: public QObject
{
    Q_OBJECT
public:
    ConnectorSE(QTableWidget*parent):QObject(parent),table(parent){}
    Q_SLOT void setOneRow(bool one){
        table->setRowCount(one?1:100);
        if(one)
        {
            if(table->item(0,0))
                table->item(0,0)->setText("1");
            else
                table->setItem(0,0,new QTableWidgetItem("1"));
        }
    }
private:
    QTableWidget*table;
};
class ComleteDelegate: public QItemDelegate
{
    Q_OBJECT
public:
    ComleteDelegate(QTableWidget*parent):QItemDelegate(parent){/*ComleteDelegate::model=nullptr;*/}
    //~ComleteDelegate();
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    //Q_SLOT void ddd();
private:
    //static QAbstractItemModel *model;
    static QSet<QString> tos;
};
class Items
{
public:
    Items(QString fieldName):fieldName(fieldName){}
    QWidget *generateEditWidget(QWidget* parent);
    QString value(QWidget* widget);
    bool isVisible(){return visible[fieldName];}
    bool isEditable(){return editable[fieldName];}
    bool isSpecial();
    bool isAutoCompleteable(){return completable[fieldName];}
    void connTo(Items toField, QWidget* fromW, QWidget* toWidget);
    QVariant lastValue(QString name, QVariant def) const;
    void setLastValue(QString name, QVariant value) const;

    //QString title(){return titles[fieldName];}
    QString title(){return fieldName;}

    static QStringList getItemList(){return itemList;}
    static QString getUserNmae();
    static QString getUser();
    static QString getCurrentGrif();
    static QString getCurrentGrifShort();
    static QStringList getGrifList();
    static QSettings* getCfg(){return cfg;}
    static QSettings* getGlobalCfg(){return gCfg;}
    static QCompleter* createCompleter(QObject* parent,QString name);
    static void updateCompleterValue(QString name,QString value);
    enum UserType{Admin,Editor,Simple,X};
    static UserType getUserType();
    static bool hasTranslation();
    static QString getUserConfigPath();
private:
    static enum UserType userType;
    QString fieldName;
    static QStringList itemList;
    //static QMap<QString,QString> titles;
    static QMap<QString,bool> visible;
    static QMap<QString,bool> editable;
    static QMap<QString,bool> completable;
    static bool inited;
    static bool init();
    static QSettings* cfg;
    static QSettings* gCfg;
};

#endif // ITEMS_H
