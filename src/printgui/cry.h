#ifndef CRY_H
#define CRT_H
char *ft_decry(char const *buf, unsigned key);
char *ft_encry(char const *buf, unsigned key);
unsigned get_key(void);
#endif
