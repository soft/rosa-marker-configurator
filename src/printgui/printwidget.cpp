#include "cry.h"
#include "printwidget.h"
#include "ui_printwidget.h"
#include <QStandardPaths>
#include <QDebug>
#include <QDir>
#include "../admingui/editor.h"
#define SUFIX ".marker.conf"
#include <QBuffer>
#include <QTextStream>
#include <QTextCodec>
#include <QDataStream>
#include <unistd.h>
#include <QRegExp>
#include <signal.h>
#include <unistd.h>
#include "items.h"
#include <QProcess>
#include <QMessageBox>

#define sep char(31)

PrintWidget::PrintWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PrintWidget),cfg(Items::getCfg())
{
    ui->setupUi(this);
    loadConfigList();
    ui->radioNoMark->setDisabled(Items::getUserType()==Items::Simple);
    connect(ui->radioNoMark,SIGNAL(toggled(bool)),this,SLOT(onFileSelectionChanged(bool)));
    ui->openEditor->setDisabled(Items::getUserType()==Items::Simple);
    connect(QApplication::instance(),SIGNAL(aboutToQuit()),this,SLOT(cancel()));
    catchUnixSignals();
}

QString PrintWidget::selectedName()
{
    QString name;
    if(ui->radioNoMark->isChecked())
        name="";
    else{
        QObjectList radios=ui->groupBox->children();
        for(int i=0;i<radios.count();i++)
        {
            QRadioButton* radio= qobject_cast<QRadioButton*>(radios.at(i));
            if(!radio || radio==ui->radioNoMark)
                continue;
            if(radio->isChecked())
            {
                name=radio->property("path").toString();
                break;
            }
        }
    }
    return name;
}

PrintWidget::~PrintWidget()
{
    delete ui;
}

void PrintWidget::loadConfigList(QString newFileName)
{
    QStringList files=Editor::fileList(false);
    QObjectList radios=ui->groupBox->children();
    for(int i=0;i<radios.count();i++)
    {
        QRadioButton* radio= qobject_cast<QRadioButton*>(radios.at(i));
        if(!radio || radio==ui->radioNoMark)
            continue;
        delete radio;
    }
    ui->radioNoMark->setChecked(Items::getUserType()!=Items::Simple);
    foreach (QString fileName, files) {
        QFile file(fileName);
        QFileInfo info(file);
        QString name=info.baseName();
        if(info.isWritable() && Items::getUserType()==Items::Simple)
            continue;//Простые пользователи не могут использовать маркировки кроме из списка подготовленых админом
        QRadioButton* radio=new QRadioButton(name,ui->groupBox);
        radio->setProperty("path",fileName);
        QBoxLayout* layout=qobject_cast<QBoxLayout*>(ui->groupBox->layout());
        if(!layout)
            break;
        layout->insertWidget(layout->count()-1,radio);
        connect(radio,SIGNAL(toggled(bool)),this,SLOT(onFileSelectionChanged(bool)));
        if(newFileName==fileName)
            radio->setChecked(true);
        else if(newFileName.isEmpty() && fileName==cfg->value("LastFile"))
            radio->setChecked(true);
    }
}

void PrintWidget::cancel()
{
    qDebug()<<"cancel print";
    printAndQuit("skip");
}

void PrintWidget::loadForm(QSettings &currentFile)
{
    QSet<QString> fieldNames;
    while(ui->formLayout->count())
    {
        ui->formLayout->removeRow(0);
    }
    while(ui->specialLayout->count())
    {
        QLayoutItem* w=ui->specialLayout->itemAt(0);
        ui->specialLayout->removeItem(w);
        delete w->widget();
        delete w;
    }
    foreach(QString group,currentFile.childGroups())
    {
        QStringList p=group.split("_");
        if(p.count()!=2 || p[0].length()<1 || p[1].length()<1)
            continue;
        currentFile.beginGroup(group);
        foreach(QString key, currentFile.childKeys())
        {
            QString line= currentFile.value(key).toString();
            QRegExp rx("<([^<>]+)>");
            int offset=-1;
            while( (offset=rx.indexIn(line,offset+1))!=-1)
            {
                QString fieldName=rx.cap(1);
                if(!fieldNames.contains(fieldName))
                {
                    fieldNames.insert(fieldName);
                    Items item(fieldName);
                    QWidget* widget;
                    if(item.isVisible() && (widget=item.generateEditWidget(this)))
                    {
                        if(!item.isEditable())
                            widget->setEnabled(false);
                        widget->setProperty("fieldName",fieldName);
                        foreach (QString to, fieldNames) {
                            QWidget* toW=findWidgetByField(to);
                            if(toW)
                            {
                                item.connTo(to,widget,toW);
                                Items(to).connTo(item,toW,widget);
                            }
                        }

                        if(item.isSpecial())
                            ui->specialLayout->addWidget(widget);
                        else
                            ui->formLayout->addRow(fieldName,widget);
                    }
                }
            }
        }
        currentFile.endGroup();
    }
    update();
}

QByteArray PrintWidget::convertToOut(QSettings& currentFile)
{
    QByteArray ba;
    QTextStream out(&ba);
    QStringList groups=currentFile.childGroups();
    foreach(QString group,groups)
    {
        QStringList p=group.split("_");
        if(p.count()!=2 || p[0].length()<1 || p[1].length()<1)
            continue;
        QString page_level=QString()+p[0][0]+p[1][0];
        currentFile.beginGroup(group);
        int index=1;
        foreach(QString key, currentFile.childKeys())
        {
            QString line= currentFile.value(key).toString();
            QRegExp rx("<([^<>]+)>");
            //int offset=-1;
            while( (/*offset=*/rx.indexIn(line,/*offset+1*/0))!=-1)
            {
                QString fieldName=rx.cap(1);
                QString value=getValueFromGui(fieldName);
                line=line.replace("<"+fieldName+">",value);
            }
            QStringList subLines=line.split("\n");
            foreach (QString subLine, subLines) {
                if(subLine.isEmpty())
                    continue;
                //QString id=page_level+QString("%1").arg(QString::number(index),2,'0');
                //out<<sep<<id<<sep<<subLine;
                writeInt(out,page_level,index,2,subLine);
                index++;
            }
        }
        currentFile.endGroup();
    }
    writeInt(out,"ML",currentFile.value("left").toInt());
    writeInt(out,"MR",currentFile.value("right").toInt());
    writeInt(out,"MT",currentFile.value("top").toInt());
    writeInt(out,"MB",currentFile.value("bottom").toInt());
    writeInt(out,"FS",currentFile.value("size").toInt());
    out<<sep<<"FN"<<currentFile.value("font").toString();
    writeInt(out,"DP",ui->doubleSide->isChecked()?1:0,1);
    out<<sep;
    out.flush();
    qDebug()<<"result"<<QString(QByteArray(ba).replace(char(31),' '));
    return ba;
}

void PrintWidget::writeInt(QTextStream &out,QString name, int val, int digits, QString line)
{
   QString id=name+QString("%1").arg(QString::number(val),digits,'0');
   out<<sep<<id;
   if(!line.isNull())
        out<<sep<<line;
}

QWidget *PrintWidget::findWidgetByField(QString fieldName)
{
    for(int row=0;row<ui->formLayout->rowCount();row++)
    {
        QWidget* widget=ui->formLayout->itemAt(row,QFormLayout::FieldRole)->widget();
        if(widget->property("fieldName").toString()==fieldName)
            return widget;
    }
    for(int row=0;row<ui->specialLayout->count();row++)
    {
        QWidget* widget=ui->specialLayout->itemAt(row)->widget();
        if(widget->property("fieldName").toString()==fieldName)
            return widget;
    }
    return nullptr;
}

QString PrintWidget::getValueFromGui(QString fieldName)
{
    return Items(fieldName).value(findWidgetByField(fieldName));
}

void PrintWidget::printAndQuit(QByteArray bout)
{
    bout.resize(4096);
    QFile out;
    if(out.open(stdout,QIODevice::WriteOnly|QIODevice::Unbuffered))
    {
        out.write(ft_encry(bout.constData(), get_key()), 4096);
        //out.write(bout);
        out.close();
    }
    else
    {
        qCritical()<<"Error write to stdout"<<out.errorString()<<write(1, ft_encry(bout.constData(), get_key()), 4096);
    }
    exit(0);
    //QApplication::instance()->exit();
}

C* c;

C::C(){
    connect(this,SIGNAL(qs()),QApplication::instance(),SLOT(quit()),Qt::QueuedConnection);
}
void PrintWidget::catchUnixSignals()
{
    c=new C;
    auto handler = [](int sig) -> void {
        Q_UNUSED(sig);
        //qDebug()<<"quit the application by signal"<<sig;
        //QCoreApplication::quit();
        c->cq();
    };

    sigset_t blocking_mask;
    sigemptyset(&blocking_mask);
    int sigs[]={SIGINT,SIGTSTP,SIGTERM};//ctrl-c, ctrl-z, kill
    for(int s=0,sig=sigs[s];s<int(sizeof(sigs)/sizeof(*sigs));s++,sig=sigs[s])//int sig=1;sig<_NSIG;sig++)
        sigaddset(&blocking_mask, sig);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask    = blocking_mask;
    sa.sa_flags   = 0;

    for(int s=0,sig=sigs[s];s<int(sizeof(sigs)/sizeof(*sigs));s++,sig=sigs[s])//int sig=1;sig<_NSIG;sig++)
        sigaction(sig, &sa, nullptr);
}

void PrintWidget::on_openEditor_clicked()
{
    Editor* editor=new Editor(selectedName());
    connect(editor,SIGNAL(anyConfigSaved(QString)),this,SLOT(loadConfigList(QString)));
    editor->setVisible(true);
}

void PrintWidget::onFileSelectionChanged(bool t)
{
    if(!t)
        return;
    QString fileName=selectedName();
    cfg->setValue("LastFile",fileName);
    QSettings currentFile(fileName,QSettings::IniFormat);
    currentFile.setIniCodec(QTextCodec::codecForLocale());
    loadForm(currentFile);
}

void PrintWidget::on_buttonBox_accepted()
{
    QString fileName=selectedName();
    if(fileName.isEmpty())
    {
        printAndQuit("no");
        return;
    }
    /*QStringList files=Editor::fileList();
    if(!files.contains(fileName))
        return;*/
    QSettings currentFile(fileName,QSettings::IniFormat);
    currentFile.setIniCodec(QTextCodec::codecForLocale());
    printAndQuit(convertToOut(currentFile));
}

void PrintWidget::on_buttonBox_rejected()
{
    cancel();
}
/*
https://gist.github.com/azadkuh/a2ac6869661ebd3f8588

#include <initializer_list>
#include <signal.h>
#include <unistd.h>

void ignoreUnixSignals(std::initializer_list<int> ignoreSignals) {
    // all these signals will be ignored.
    for (int sig : ignoreSignals)
        signal(sig, SIG_IGN);
}

void catchUnixSignals(std::initializer_list<int> quitSignals) {
    auto handler = [](int sig) -> void {
        // blocking and not aysnc-signal-safe func are valid
        printf("\nquit the application by signal(%d).\n", sig);
        QCoreApplication::quit();
    };

    sigset_t blocking_mask;
    sigemptyset(&blocking_mask);
    for (auto sig : quitSignals)
        sigaddset(&blocking_mask, sig);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask    = blocking_mask;
    sa.sa_flags   = 0;

    for (auto sig : quitSignals)
        sigaction(sig, &sa, nullptr);
}

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    catchUnixSignals({SIGQUIT, SIGINT, SIGTERM, SIGHUP});

    // do your initialization and other stuff


    return app.exec();
}
  */

void PrintWidget::on_openAdmEditor_clicked()
{
    /*qDebug()<<"setuid"<<setuid(0);
    qDebug()<<"setreuid"<<setreuid(0,0);
    qDebug()<<"new user type"<<Items::getUserType()<<QFileInfo ("/etc/").isWritable();*/

    QMessageBox::information(this,"Маркировка документов","Для редактирования общих режимов маркировки воспользуйтесь соответствующим пунктом в главном меню ОС");
    return;

    //Try to switch to root
    QProcess* proc=new QProcess(this);
    QString cmd="gksu -m \"Введите пароль администратора (root) для редактирования общих шаблонов маркировки печати\" "+QCoreApplication::instance()->applicationFilePath()+" admin";
    proc->start(cmd);
    qDebug()<<"Starting editor:"<<cmd<<proc->state();
    connect(QCoreApplication::instance(),SIGNAL(aboutToQuit()),proc,SLOT(kill()));
    connect(proc,SIGNAL(finished(int)),this,SLOT(onEditorFinished()));
}

void PrintWidget::onEditorFinished()
{
    QProcess* proc=qobject_cast<QProcess*>(sender());
    if(proc)
        qDebug()<<"editor:"<<proc->readAllStandardError();
}
