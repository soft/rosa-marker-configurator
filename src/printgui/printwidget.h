#ifndef PRINTWIDGET_H
#define PRINTWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QTextStream>
namespace Ui {
class PrintWidget;
}
class QAbstractButton;
class PrintWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PrintWidget(QWidget *parent = 0);
    QString selectedName();
    ~PrintWidget();

private slots:
    void on_openEditor_clicked();
    void onFileSelectionChanged(bool t);
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();
    void loadConfigList(QString fileNmae="");
    void cancel();

    void on_openAdmEditor_clicked();
    Q_SLOT void onEditorFinished();

private:
    Ui::PrintWidget *ui;
    void loadForm(QSettings &currentFile);
    QByteArray convertToOut(QSettings &currentFile);
    void writeInt(QTextStream& out, QString name, int val, int digits=2, QString line=QString());
    QWidget* findWidgetByField(QString fieldName);
    QString getValueFromGui(QString fieldName);
    void printAndQuit(QByteArray out);
    void catchUnixSignals();
    QSettings* cfg;
};
class C:public QObject
{
    Q_OBJECT
    static C c;
    Q_SIGNAL void qs();
public:
    C();
    void cq()
    {
        emit qs();
    }
    /*static void q()
    {
        c.cq();
    }*/
};

#endif // PRINTWIDGET_H
