#ifndef EDITOR_H
#define EDITOR_H

#include <QWidget>
#include <QSettings>
#include <QMap>
#include <QMenu>

namespace Ui {
class Editor;
}
class QAbstractButton;
class QListWidgetItem;
class Editor : public QWidget
{
    Q_OBJECT

public:
    explicit Editor(QString name="", QWidget *parent = nullptr);
    ~Editor();
    static QStringList sysPathList(bool personal);
    static QStringList fileList(bool onlyWriteable);
    void setUiFileList(QStringList fileList, int index);
    static bool isDir(QString name);
    static QString displayNmae(QString fileNmae);
    QString currentPage() const;
signals:
    void anyConfigSaved(QString fileNmae);
private slots:

    void on_pages_itemChanged(QListWidgetItem *item);

    void on_pages_itemSelectionChanged();

    void on_font_currentFontChanged(const QFont &f);

    void on_deleteButton_clicked();

    void on_grifsButton_clicked();

    void on_comboConfigs_editTextChanged(const QString &arg1);

    void on_newButton_clicked();

private:
    Ui::Editor *ui;
    Q_SLOT void onChanged();
    Q_SLOT void onConfigChanged();
    Q_SLOT void onTextChanged();
    Q_SLOT void onButtonClicked(QAbstractButton*);
    void switchTo(QString fileName);
    void loadConfig(QString fileName);
    void updateState();
    void loadUiRawTexts();
    void save();
    QString getCurrentFileName();
    bool isPageChecked(QString itemPage);
    QString pageAt(int index) const;
    QString itemPage(QListWidgetItem* item) const;
    void fixAlignment();
    bool exist;
    bool changed;
    QStringList filesR;
    QStringList filesRW;

    QSettings cfg;
    QMap<QString, QString> topRawTexts;
    QMap<QString, QString> bottomRawTexts;
    int lastConfigComboIndex;

    QMenu grifsMenu;
};


#endif // EDITOR_H
