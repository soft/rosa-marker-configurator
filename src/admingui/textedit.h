#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QTextEdit>


class TextEdit : public QTextEdit
{
    Q_OBJECT
public:
    explicit TextEdit(QWidget *parent = nullptr);
    explicit TextEdit(const QString &text, QWidget *parent = nullptr);

protected:
    bool canInsertFromMimeData(const QMimeData *source) const;
    void insertFromMimeData(const QMimeData *source);

};

#endif // TEXTEDIT_H
