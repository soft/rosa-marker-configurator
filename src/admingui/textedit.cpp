#include "textedit.h"
#include <QMimeData>
#include <QDebug>

TextEdit::TextEdit(QWidget *parent):QTextEdit(parent)
{

}

TextEdit::TextEdit(const QString &text, QWidget *parent):QTextEdit(text,parent)
{

}

bool TextEdit::canInsertFromMimeData(const QMimeData *source) const
{
    return source->hasText() || source->hasFormat("application/x-qabstractitemmodeldatalist");
}

void TextEdit::insertFromMimeData(const QMimeData *source)
{
    QTextCursor cursor = this->textCursor();
    //QTextDocument *document = this->document();
    QString text;
    //document->addResource(QTextDocument::ImageResource, QUrl("image"), image);
    //cursor.insertText("<"+source->text()+">");
    if(source->hasText())
        text=source->text();
    else if(source->hasFormat("application/x-qabstractitemmodeldatalist"))
    {
        QByteArray data=source->data("application/x-qabstractitemmodeldatalist");
        QDataStream stream(&data, QIODevice::ReadOnly);
        if (!stream.atEnd())
        {
            int row, col;
            QMap<int,  QVariant> roleDataMap;
            stream >> row >> col >>roleDataMap;
            text=roleDataMap[Qt::DisplayRole].toString();
        }
    }
    cursor.insertText("<"+text+">");
}
