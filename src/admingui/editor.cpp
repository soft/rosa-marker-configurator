#include "editor.h"
#include "ui_editor.h"
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QTextCodec>
#define SUFIX ".marker.conf"
#include "../printgui/items.h"
#include <QStandardItemModel>

Editor::Editor(QString name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Editor),cfg("rosa","PrintMarker")
{
    ui->setupUi(this);
    //ui->textEditTop->document()->setDefaultTextOption(QTextOption(Qt::AlignRight));

    //ui->textEditTop->setAlignment(Qt::AlignRight);
    connect(ui->size,SIGNAL(valueChanged(int)),this,SLOT(onChanged()));
    connect(ui->top,SIGNAL(valueChanged(int)),this,SLOT(onChanged()));
    connect(ui->bottom,SIGNAL(valueChanged(int)),this,SLOT(onChanged()));
    connect(ui->left,SIGNAL(valueChanged(int)),this,SLOT(onChanged()));
    connect(ui->right,SIGNAL(valueChanged(int)),this,SLOT(onChanged()));
    connect(ui->textEditTop,SIGNAL(textChanged()),this,SLOT(onChanged()));
    connect(ui->textEditBottom,SIGNAL(textChanged()),this,SLOT(onChanged()));
    connect(ui->textEditTop,SIGNAL(textChanged()),this,SLOT(onTextChanged()));
    connect(ui->textEditBottom,SIGNAL(textChanged()),this,SLOT(onTextChanged()));

    connect(ui->comboConfigs,SIGNAL(currentIndexChanged(int)),this,SLOT(onConfigChanged()));
    connect(ui->buttonBox,SIGNAL(clicked(QAbstractButton*)),this,SLOT(onButtonClicked(QAbstractButton*)));

    //QStringList fonts=QFontDatabase().families(QFontDatabase::Cyrillic);
    ui->font->setWritingSystem(QFontDatabase::Cyrillic);
    ui->font->setFontFilters(QFontComboBox::MonospacedFonts);
    ui->tags->clear();
    foreach (QString tag, Items::getItemList()) {
        ui->tags->addItem(tag);
    }

    /*foreach (QString grif, Items::getGrifList()) {
        QAction* a=new QAction(grif,ui->grifsButton);
        a->setCheckable(true);
        ui->grifsButton->insertAction(0,a);

    }
    ui->grifsButton->setContextMenuPolicy(Qt::ActionsContextMenu);*/

    foreach (QString grif, Items::getGrifList()) {
        //QAction* a=new QAction(grif);
        QAction* a=grifsMenu.addAction(grif);
        a->setEnabled(Items::getUserType()==Items::Admin);
        a->setCheckable(true);
    }
/*    QMenu*menu=new QMenu(this);
    //QPushButton*menu=new QPushButton(this);
    ui->toplLayout->insertWidget(2,menu);
    //QAction* a=menu->addAction("213");
    //a->setCheckable(true);

    QStandardItemModel* model=new QStandardItemModel(Items::getGrifList().count(), 1); // 3 rows, 1 col
        for (int r = 0; r < model->rowCount(); ++r)
        {
            QStandardItem* item = new QStandardItem(Items::getGrifList()[r]);

            item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);

            item->setData(Qt::Checked, Qt::CheckStateRole);

            model->setItem(r, 0, item);
        }
    ui->grifCombo->setModel(model);
    foreach (QString grif, Items::getGrifList()) {
        ui->grifCombo->addItem(grif);
        ui->grifCombo->setItemData(ui->grifCombo->count()-1,Qt::Unchecked, Qt::CheckStateRole);
    }*/

    if(name.isEmpty() && parent==nullptr && Items::getUserType()!=Items::Admin)
    {
        QMessageBox::critical(this,"Ошибка","Для конфигурации режимов печати необходимы права администратора");
        exit(1);
    }
    filesR=fileList(false);
    filesRW=fileList(true);
    switchTo(name);
}

Editor::~Editor()
{
    delete ui;
}

QStringList Editor::sysPathList(bool personal)
{
    QStringList pathList;
    if(!personal)
    {
        pathList=QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation);
        if(!personal)
        {//Убрать все личные папки админа, ему личные шаблоны не сохранять
            foreach (QString path, pathList)
                if(!path.startsWith("/etc"))
                    pathList.removeOne(path);
        }
    }
    else if(Items::getUserType()!=Items::Simple)
    {
        QString homecfg = Items::getUserConfigPath();
        pathList.append(QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation));//Теперь тут получаются пути рута а не пользователя
        if(!pathList.contains(homecfg))
            pathList.insert(0,homecfg);
        //qDebug()<<"writableLocations:"<<QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation)<<homedir;
    }
    return pathList;
}

QStringList Editor::fileList(bool onlyWriteable)
{
    QStringList files;
    QStringList pathList;
    if(onlyWriteable)
        pathList=sysPathList(Items::getUserType()!=Items::Admin);
    else
    {
        pathList=sysPathList(false);
        pathList.append(sysPathList(true));
    }
    foreach (QString path, pathList) {
        QDir dir(path);
        QStringList localFiles=dir.entryList(QStringList()<<QString("*")+SUFIX, QDir::Files | QDir::Readable);
        foreach (QString file, localFiles) {
            files.append(dir.filePath(file));
        }
    }
    return files;
}

void Editor::setUiFileList(QStringList fileList,int index)
{
    ui->comboConfigs->blockSignals(true);
    ui->comboConfigs->clear();
    foreach (QString fileName, fileList) {
        if(isDir(fileName))
            ui->comboConfigs->addItem("<Новый>",fileName);
        else
            ui->comboConfigs->addItem(displayNmae(fileName),fileName);
    }
    ui->comboConfigs->setCurrentIndex(index);
    ui->comboConfigs->blockSignals(false);
    lastConfigComboIndex=index;
}

bool Editor::isDir(QString name)
{
    QFileInfo info(name);
    return info.isDir();
}

QString Editor::displayNmae(QString fileNmae)
{
    QFileInfo info(fileNmae);
    return info.baseName();
}

QString Editor::currentPage() const
{
    QList<QListWidgetItem*> selection=ui->pages->selectedItems();
    if(selection.count()<1)
        return "";
    QListWidgetItem* item=selection.at(0);
    return itemPage(item);
}

void Editor::onChanged()
{//on any changed by gui
    changed=true;
    updateState();
}

void Editor::onConfigChanged()
{
    if(changed && !exist)
    {
        ui->comboConfigs->blockSignals(true);
        ui->comboConfigs->setCurrentIndex(lastConfigComboIndex);
        ui->comboConfigs->blockSignals(false);
        QMessageBox::information(this,"","Сохраните или отклоните изменения в текущей конфигурации");
        return;
    }
    else
    {
        exist=!isDir(getCurrentFileName());
        loadConfig(getCurrentFileName());
    }
}

void Editor::onTextChanged()
{
    QString page=currentPage();
    QString textT,textB;
    textT=ui->textEditTop->document()->toPlainText();
    textB=ui->textEditBottom->document()->toPlainText();
    QRegExp r("([^\\x0001-\\x007fа-яА-ЯЁё№])");
    if(r.indexIn(textT)!=-1 || r.indexIn(textB)!=-1)
    {
        QMessageBox::critical(this,"Ошибка","Недопустимый символ "+r.cap(1)+" в тексте. Допустимы ascii-символы, русские буквы и символ номера");
        loadUiRawTexts();
        return;
    }
    topRawTexts[page]=textT;
    bottomRawTexts[page]=textB;
    if(!topRawTexts[page].trimmed().isEmpty() || !bottomRawTexts[page].trimmed().isEmpty())
    {
        for(int i=0;i<ui->pages->count();i++)
            if(pageAt(i)==page)
                ui->pages->item(i)->setCheckState(Qt::Checked);
    }
}

void Editor::onButtonClicked(QAbstractButton * button)
{
    switch (ui->buttonBox->standardButton(button)) {
    case QDialogButtonBox::Save:
        save();
        break;
    case QDialogButtonBox::Discard:
        loadConfig(ui->comboConfigs->currentData().toString());
        changed=false;
        updateState();
        break;
    default:
        break;
    }

}

void Editor::switchTo(QString name)
{
    QStringList pathList=sysPathList(Items::getUserType()==Items::Editor);
    QString newFilePath=pathList.isEmpty()?"":pathList.at(0);
    if(Items::getUserType()==Items::Simple || newFilePath.isEmpty())
    {//Файлы править и создавать нельзя, открываем для просмотра первый попавшийся
        setUiFileList(filesR,0);
        exist=filesR.count()>0;
        changed=false;
        loadConfig(ui->comboConfigs->currentData().toString());
    }
    else if(name.isEmpty())
    {//Заведомо новый шаблон
        filesRW.insert(0,newFilePath);
        setUiFileList(filesRW,0);
        ui->comboConfigs->setEditable(true);
        exist=false;
        changed=false;
        loadConfig(newFilePath);
    }
    else if(filesRW.contains(name))
    {//Открывается существующий, который можно править
        setUiFileList(filesRW,filesRW.indexOf(name));
        ui->comboConfigs->setEditable(false);
        exist=true;
        changed=false;
        loadConfig(name);
    } else if(filesR.contains(name))
    {//Открывается только для чтения и создается новый безимянный
        filesRW.insert(0,newFilePath);
        setUiFileList(filesRW,0);
        ui->comboConfigs->setEditable(true);
        changed=false;
        exist=true;
        loadConfig(name);
        exist=false;
    }else if(!name.contains("/"))
    {//создать не существующий файл
        //name=newFilePath+"/"+name+SUFIX;
        name=QDir(newFilePath).filePath(name+SUFIX);
        filesRW.insert(0,name);
        setUiFileList(filesRW,0);
        ui->comboConfigs->setEditable(true);
        exist=false;
        changed=false;
        loadConfig(newFilePath);
    }
    else
    {
        QMessageBox::warning(this,"Ошибка","Конфигурация "+name+ " не найдена");
        filesRW.insert(0,newFilePath);
        setUiFileList(filesRW,0);
        ui->comboConfigs->setEditable(true);
        exist=false;
        changed=false;
        loadConfig(newFilePath);
    }
    updateState();
}

void Editor::loadConfig(QString fileName)
{

    ui->font->blockSignals(true);
    ui->size->blockSignals(true);
    ui->top->blockSignals(true);
    ui->bottom->blockSignals(true);
    ui->left->blockSignals(true);
    ui->right->blockSignals(true);

    QVariantList grifList=Items::getGlobalCfg()->value("grifs").toList();
    for(int i=0;i<grifsMenu.actions().count();i++)
    {
        QAction* a=grifsMenu.actions().at(i);
        if(grifList.count()>= i+1)
            a->setChecked(grifList[i].toBool());
        else
            a->setChecked(i>1);
    }


    if(isDir(fileName) || !exist)
    {
        ui->font->setCurrentFont(QFont(cfg.value("font",QFont().family()).toString()));
        ui->size->setValue(cfg.value("size",QFont().pointSize()).toInt());
        ui->top->setValue(cfg.value("top",5).toInt());
        ui->bottom->setValue(cfg.value("bottom",5).toInt());
        ui->left->setValue(cfg.value("left",5).toInt());
        ui->right->setValue(cfg.value("right",5).toInt());
        for(int i=0;i<ui->pages->count();i++)
        {
            ui->pages->blockSignals(true);
            QString page=pageAt(i);
            if(page=="other")
            {
                ui->pages->item(i)->setCheckState(  Qt::Checked );
                ui->pages->item(i)->setSelected(true);
            }
            else
            {
                ui->pages->item(i)->setCheckState(  Qt::PartiallyChecked );
            }
            ui->pages->blockSignals(false);
        }
        topRawTexts.clear();
        bottomRawTexts.clear();
    }
    else
    {
        QSettings currentFile(fileName,QSettings::IniFormat);
        currentFile.setIniCodec(QTextCodec::codecForLocale());
        ui->font->setCurrentFont(QFont(currentFile.value("font",QFont().family()).toString()));
        ui->size->setValue(currentFile.value("size",QFont().pointSize()).toInt());
        ui->top->setValue(currentFile.value("top",5).toInt());
        ui->bottom->setValue(currentFile.value("bottom",5).toInt());
        ui->left->setValue(currentFile.value("left",5).toInt());
        ui->right->setValue(currentFile.value("right",5).toInt());
        QStringList groups=currentFile.childGroups();
        ui->pages->blockSignals(true);
        ui->pages->clearSelection();
        for(int i=0;i<ui->pages->count();i++)
        {
            QString page=pageAt(i);
            ui->pages->item(i)->setCheckState(  Qt::Unchecked );
            if(groups.contains(page+"_top"))
            {
                ui->pages->item(i)->setCheckState(Qt::Checked);
                QString text;
                currentFile.beginGroup(page+"_top");
                foreach(QString key, currentFile.childKeys())
                {
                    text+=currentFile.value(key).toString();
                    text+="\n";
                }
                topRawTexts[page]=text;
                currentFile.endGroup();
                if(ui->pages->selectedItems().isEmpty())
                    ui->pages->item(i)->setSelected(true);
            }
            if(groups.contains(page+"_bottom"))
            {
                ui->pages->item(i)->setCheckState(Qt::Checked);
                QString text;
                currentFile.beginGroup(page+"_bottom");
                foreach(QString key, currentFile.childKeys())
                {
                    text+=currentFile.value(key).toString();
                    text+="\n";
                }
                bottomRawTexts[page]=text;
                topRawTexts[page]=topRawTexts[page];//чтобы появился ключ, если его нет
                currentFile.endGroup();
                if(ui->pages->selectedItems().isEmpty())
                    ui->pages->item(i)->setSelected(true);
            }
            //ui->pages->item(i)->setCheckState(  Qt::PartiallyChecked );
        }
        ui->pages->blockSignals(false);
    }
    ui->font->blockSignals(false);
    ui->size->blockSignals(false);
    ui->top->blockSignals(false);
    ui->bottom->blockSignals(false);
    ui->left->blockSignals(false);
    ui->right->blockSignals(false);
    loadUiRawTexts();
}

void Editor::updateState()
{
    //ui->comboConfigs->setDisabled(changed && exist);
    ui->buttonBox->setEnabled(true);
}
#include <QAbstractTextDocumentLayout>
#include <QTextBlock>
void Editor::loadUiRawTexts()
{
    ui->textEditTop->blockSignals(true);
    ui->textEditBottom->blockSignals(true);
    QString page=currentPage();
    if(topRawTexts.contains(page))
        ui->textEditTop->document()->setPlainText(topRawTexts[page]);
    else
        ui->textEditTop->document()->setPlainText("");
    if(bottomRawTexts.contains(page))
        ui->textEditBottom->document()->setPlainText(bottomRawTexts[page]);
    else
        ui->textEditBottom->document()->setPlainText("");
    //ui->textEditTop->setAlignment(Qt::AlignRight);
    //doc - block - textoption -textlayout - alignment
    //ui->textEditTop->document()->documentLayout()->set
    //QTextBlock block=ui->textEditTop->document()->begin();
    ui->textEditTop->setFont(ui->font->currentFont());
    ui->textEditBottom->setFont(ui->font->currentFont());
    fixAlignment();
    ui->textEditTop->blockSignals(false);
    ui->textEditBottom->blockSignals(false);
}

void Editor::save()
{
    QVariantList grifList;
    foreach (QAction* a, grifsMenu.actions()) {
        grifList.append(a->isChecked());
    }
    Items::getGlobalCfg()->setValue("grifs",(grifList));
    Items::getGlobalCfg()->sync();

    QString currentFileName=getCurrentFileName();
    QString userInputName=ui->comboConfigs->currentText();
    if(userInputName.isEmpty() || userInputName.contains("<") || userInputName.contains(">"))
    {
        QMessageBox::critical(this,"Ошибка","Необходимо ввести имя");
        return;
    }
    /*if(!exist)
    {
        if(!isDir(currentFileName))
        {
            currentFileName=QFileInfo(currentFileName).dir().absolutePath();
        }
        //currentFileName теперь точно dir
        qDebug()<<"dir"<<currentFileName;
        currentFileName=QDir(currentFileName).filePath(userInputName+SUFIX);
        qDebug()<<"file"<<currentFileName;
        //currentFileName+="/"+userInputName+SUFIX;
        filesRW[0]=currentFileName;
        setUiFileList(filesRW,0);
    }*/
    QFile::remove(currentFileName);
    QSettings currentFile(currentFileName,QSettings::IniFormat);
    currentFile.setIniCodec(QTextCodec::codecForLocale());
    if(!currentFile.isWritable())
    {
        QMessageBox::critical(this,"Ошибка","Невозможно записать файл "+currentFileName);
        return;
    }
    currentFile.setValue("font",ui->font->currentFont().family());
    currentFile.setValue("size",ui->size->value());
    currentFile.setValue("top",ui->top->value());
    currentFile.setValue("bottom",ui->bottom->value());
    currentFile.setValue("left",ui->left->value());
    currentFile.setValue("right",ui->right->value());
    foreach (QString page, topRawTexts.keys()) {
        if(!isPageChecked(page))
            continue;
        qDebug()<<"save used page"<<page;
        currentFile.beginGroup(page+"_top");
        QStringList topList=topRawTexts[page].split("\n");
        if(topList.last().trimmed().isEmpty())
            topList.removeLast();
        for (int i = 0; i < topList.size(); ++i) {
            currentFile.setValue(QString::number(i),topList.at(i));
        }
        currentFile.endGroup();
        currentFile.beginGroup(page+"_bottom");

         QStringList bottomList=bottomRawTexts[page].split("\n");
         if(bottomList.last().trimmed().isEmpty())
             bottomList.removeLast();
          for (int i = 0; i < bottomList.size(); ++i) {
              currentFile.setValue(QString::number(i),bottomList.at(i));
          }
        currentFile.endGroup();
    }
    currentFile.sync();//При выходе оно и само сохранится, но для emit anyConfigSaved важно чтоб файл сохранился раньше
    emit anyConfigSaved(currentFileName);
    changed=false;
    exist=true;
    filesR=fileList(false);
    filesRW=fileList(true);
    switchTo(currentFileName);
    updateState();
}

QString Editor::getCurrentFileName()
{
    QString name=ui->comboConfigs->currentText();
    QString from=ui->comboConfigs->currentData().toString();
    QDir dir;
    if(QFileInfo(from).isDir())
        dir=from;
    else
        dir=QFileInfo(from).dir();
    return dir.filePath(name+SUFIX);
}

bool Editor::isPageChecked(QString page)
{
    for(int i=0;i<ui->pages->count();i++)
    {
        QString ipage=pageAt(i);
        if(ipage==page)
            return ui->pages->item(i)->checkState()==Qt::Checked;
    }
    qDebug()<<"page "<<page<<"not found";
    return true;//В этой версии программы нет такой страницы. Значит хотябы не будем её удалять при редактировании
}

QString Editor::pageAt(int index) const
{
    return itemPage(ui->pages->item(index));
}

QString Editor::itemPage(QListWidgetItem *item) const
{
    QString page=item->data(Qt::StatusTipRole).toString();
    return page;
}

void Editor::fixAlignment()
{
    QString page=currentPage();
    if(page.startsWith("b"))
    {
        ui->textEditTop->document()->setDefaultTextOption(QTextOption(Qt::AlignLeft));
        ui->textEditBottom->document()->setDefaultTextOption(QTextOption(Qt::AlignLeft));
    }
    else
    {
        ui->textEditTop->document()->setDefaultTextOption(QTextOption(Qt::AlignRight));
        ui->textEditBottom->document()->setDefaultTextOption(QTextOption(Qt::AlignRight));
    }

    return ;
    for(
        QTextBlock block=ui->textEditTop->document()->begin();
        block!=ui->textEditTop->document()->end();
        block=block.next()
    )
        block.layout()->setTextOption(QTextOption(Qt::AlignRight));

}



void Editor::on_pages_itemChanged(QListWidgetItem *item)
{
    if(item->checkState()==Qt::Unchecked)
    {
        QString page=itemPage(item);
        topRawTexts.remove(page);
        bottomRawTexts.remove(page);
        loadUiRawTexts();
    }
}

void Editor::on_pages_itemSelectionChanged()
{
    loadUiRawTexts();
}


void Editor::on_font_currentFontChanged(const QFont &f)
{
    ui->textEditTop->setFont(f);
    fixAlignment();
    ui->textEditBottom->setFont(f);
}

void Editor::on_deleteButton_clicked()
{
    QString fileName=getCurrentFileName();
    if(exist && !isDir(fileName))
    {
        int r=QMessageBox::question(this,"Удаление шаблона","Удалить шаблон "+displayNmae(fileName)+"?",QMessageBox::Yes|QMessageBox::No);
        if(r==QMessageBox::Yes)
        {
            QFile file(fileName);
            if(!file.remove())
            {
                QMessageBox::warning(this,"Ошибка","Не удалось удалить файл\n"+file.errorString());
            }
            filesR=fileList(false);
            filesRW=fileList(true);
            switchTo("");
            emit anyConfigSaved("");
        }
    }
    else
    {
        filesR=fileList(false);
        filesRW=fileList(true);
        switchTo("");
    }
}

void Editor::on_grifsButton_clicked()
{
//    ui->grifsButton->setContextMenuPolicy();
    //QMenu menu;


    //grifsMenu.exec(ui->grifsButton->parentWidget()->mapToGlobal(ui->grifsButton->geometry().bottomLeft()));
    grifsMenu.popup(ui->grifsButton->parentWidget()->mapToGlobal(ui->grifsButton->geometry().bottomLeft()));
}

void Editor::on_comboConfigs_editTextChanged(const QString &arg1)
{
    /*if(exist)
    {
        if(ui->comboConfigs->currentIndex()!=0)
        {
            QString name=ui->comboConfigs->currentData().toString()+arg1;
            qDebug()<<"new name"<<name;
            switchTo(name);
        }
    }*/
}

void Editor::on_newButton_clicked()
{
    filesR=fileList(false);
    filesRW=fileList(true);
    switchTo("");
}
