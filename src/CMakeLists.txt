cmake_minimum_required(VERSION 3.2)
#project(marker_configurator)

#set(CMAKE_INSTALL_PREFIX /usr/sbin)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Core REQUIRED)

set(CMAKE_AUTOMOC TRUE)
set(CMAKE_AUTOUIC TRUE)
set(CMAKE_AUTORCC TRUE)

set(CMAKE_INCLUDE_CURRENT_DIR TRUE)
set(CMAKE_CXX_STANDARD 11)

add_subdirectory(admingui)
add_subdirectory(message)
add_subdirectory(printgui)

#install(FILES ../PrintMarker.conf DESTINATION /etc/xdg/rosa)
#install(FILES ../Гос\ маркировка.marker.conf DESTINATION /etc/xdg)
